<?php
  /*-------------------------
  Autor: Jesús Caballero P.
  Facebook: https://www.facebook.com/unicoescritorbeats
  Mail: uebeats@gmail.com
  ---------------------------*/
    /* Conectar con Base de datos */
    require_once 'includes/conexion.php';//Contiene funcion que conecta a la base de datos

    /* Se ejecuta un require_once a la libreria SDK de TRANSBANK */
    require_once './vendor/autoload.php';

    /* Importamos las clases */
    use Transbank\Webpay\Webpay;
    use Transbank\Webpay\Configuration;

    /* Inicializacion de $transaction Webpay */
    $transaction = (new Webpay(Configuration::forTestingWebpayPlusNormal()))->getNormalTransaction();

    $tokenWs = filter_input(INPUT_POST, 'token_ws');
    $result = $transaction->getTransactionResult($tokenWs);

      $output = $result->detailOutput;
        if ($output->responseCode == 0) {
            // Transaccion exitosa, puedes procesar el resultado
            // con el contenido de las variables $result y $output.
            echo 'Orden de Compra:'. $result->buyOrder .'<br>';
            echo 'Session Id:'. $result->sessionId .'<br>';
            echo 'Numero Tarjeta:'. $result->cardDetail->cardNumber .'<br>';
            echo 'Expiracion:'. $result->cardDetail->cardExpirationDate .'<br>';
            echo 'Fecha MMDD:'. $result->accountingDate .'<br>';
            echo 'Fecha y Hora:'. $result->transactionDate .'<br>';
            echo 'url Redirection:'. $result->urlRedirection .'<br>';
            echo 'Codigo autorizacion:'. $output->authorizationCode .'<br>';
            echo 'Tipo de Pago:'. $output->paymentTypeCode[0] .'<br>';
            echo 'Monto:'. $output->amount .'<br>';
            echo 'Cuotas:'. $output->sharesNumber .'<br>';
            echo 'Code comercio:'. $output->commerceCode .'<br>';
            echo 'Orden de Compra:'. $output->buyOrder .'<br>';
        }
    // $output = $result->detailOutput;
    //   if ($output->responseCode == 0) {
    //   // Transaccion exitosa, puedes procesar el resultado con el contenido de
    //   // las variables result y output.
    //     echo '<script>localStorage.clear();</script>';
    //     echo '<script>localStorage.setItem("buyOrder",'. $output->buyOrder .');</script>';
    //     echo '<script>localStorage.setItem("accoutingDate",'. $output->accoutingDate .');</script>';
    //     echo '<script>localStorage.setItem("paymentType",'. $output->paymentTypeCode .');</script>';
    //     echo '<script>localStorage.setItem("authorizationCode",'. $output->authorizationCode .');</script>';
    //     echo '<script>localStorage.setItem("amount", '. $output->amount .');</script>';
    //     echo '<script>localStorage.setItem("responseCode", '. $output->responseCode .');</script>';

    //     $number_order = $output->buyOrder;
    //     $q = "UPDATE order_water SET status_order = 'pagado' WHERE number_order = ".$number_order;
    //     $r = $con->query($q);

    //     echo $output->buyOrder .'<br>';
    //     echo $result->accoutingDate .'<br>';
    //     echo $output->paymentTypeCode .'<br>';
    //     echo $output->authorizationCode .'<br>';
    //     echo $output->amount .'<br>';
    //     echo $output->responseCode .'<br>';
        
    //   }
?>
<?php if($output->responseCode == 0) : ?>

<form action="<?php echo $result->urlRedirection;?>" method="post" id="return-form">
      <input type="hidden" name="token_ws" value="<?php echo $tokenWs;?>">
</form>


<script>
 //document.getElementById('return-form').submit();
</script>
<?php endif;?>