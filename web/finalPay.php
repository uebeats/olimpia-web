<!DOCTYPE html>
<html lang="es_CL">
    <head>
        <?php $title = "Comprobante de Pago"; include 'head.php';?>
    <style>.container {max-width: 960px;}.lh-condensed{line-height:1.25;}
    </style>
    </head>
  <body class="bg-light">
    <div class="container">
  <div class="py-5 text-center">
    <a href="index.php"><img class="d-block mx-auto mb-4" src="images/isotipo-png.png" alt="" width="72" height="72"></a>
    
    <h2>Comprobante de Pago </h2>
    <p class="lead">Gracias por elegirnos como tu proveedor de Agua 110% Purificada, <br>dentro de unos minutos llegara a tu correo un comprobante del pago.</p>
    <div id="alertSuccess" class="alert alert-success" role="alert">
        El pago ha sido confirmado, satisfactoriamente. Y ya se encuentra en proceso de despacho!
    </div>
  </div>

  <div class="row">
    <div class="col-md-8 offset-md-2  order-md-2 mb-4">
      <h4 class="d-flex justify-content-between align-items-center mb-3">
        <span class="text-muted">Pedido N°<span id="buyOrder"></span></span>
        <!-- <span class="badge badge-secondary badge-pill">3</span> -->
      </h4>
    <!-- Listado Productos -->
      <ul class="list-group mb-3">

        <li class="list-group-item d-flex justify-content-between lh-condensed">
          <div>
            <h6 class="my-0">Detalles de la Transacción</h6>
            <small class="text-muted">Monto:</small><br>
            <small class="text-muted">Fecha:</small><br>
            <small class="text-muted">Tipo de pago:</small><br>
            <small class="text-muted">Código de Autorización:</small><br>
            <small class="text-muted">Código de Respuesta:</small><br>
          </div>
          <div>
            <h6 class="my-0">#</h6>
            <small class="text-mute">$<span id="amount" ></span></small><br>
            <small id="transactionDate" class="text-muted"></small><br>
            <small id="paymentType" class="text-muted"></small><br>
            <small id="authorizationCode" class="text-muted"></small><br>
            <small id="responseCode" class="text-muted"></small><br>
          </div>
        </li>

      </ul>
      <ul class="list-group mb-3">
        <li class="list-group-item d-flex justify-content-between lh-condensed">
          <div>
            <h6 class="my-0">Product name</h6>
            <small class="text-muted">Brief description</small>
          </div>
          <span class="text-muted">$12</span>
        </li>
        <li class="list-group-item d-flex justify-content-between lh-condensed">
          <div>
            <h6 class="my-0">Second product</h6>
            <small class="text-muted">Brief description</small>
          </div>
          <span class="text-muted">$8</span>
        </li>
        <li class="list-group-item d-flex justify-content-between lh-condensed">
          <div>
            <h6 class="my-0">Third item</h6>
            <small class="text-muted">Brief description</small>
          </div>
          <span class="text-muted">$5</span>
        </li>
        <li class="list-group-item d-flex justify-content-between bg-light">
          <div class="text-success">
            <h6 class="my-0">Promo code</h6>
            <small>EXAMPLECODE</small>
          </div>
          <span class="text-success">-$5</span>
        </li>
        <li class="list-group-item d-flex justify-content-between">
          <span>Total (USD)</span>
          <strong>$20</strong>
        </li>
      </ul>
      <!-- /Fin Lista Productos -->

      <div class="">
        <button class="btn btn-secondary"><i class="fa fa-print"></i> Imprimir </button>
      </div>
    </div>
  </div>

  <footer class="my-5 pt-5 text-muted text-center text-small">
    <div class="col-lg-auto mt-4">
        <p class="text-black"><img src="images/logo-olimpia-web.png"><small>Una empresa <a href="#" class="text-black ">Parentesis</a></small></p>
    </div>
  </footer>
</div>
    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Custom scripts for this template -->
    <!-- <script src="js/style.js"></script> -->
    <script>

        document.getElementById('buyOrder').innerHTML = localStorage.getItem('buyOrder');
        document.getElementById('amount').innerHTML = localStorage.getItem('amount');
        document.getElementById('transactionDate').innerHTML = localStorage.getItem('transactionDate');
        document.getElementById('paymentType').innerHTML = localStorage.getItem('paymentTypeCode');
        document.getElementById('authorizationCode').innerHTML = localStorage.getItem('authorizationCode');
        document.getElementById('responseCode').innerHTML = localStorage.getItem('responseCode');

    </script>
</body>
</html>