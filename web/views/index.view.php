<!DOCTYPE html>
<html lang="es_CL">

<head>
    <?php include 'head.php';?>
    <style>
        #loading {display:none;}
        
        /* equal card height */
        .row-equal > div[class*='col-'] {
            display: flex;
            flex: 1 0 auto;
        }

        .row-equal .card {
        width: 100%;
        }

        /* ensure equal card height inside carousel */
        .carousel-inner>.row-equal.active, 
        .carousel-inner>.row-equal.next, 
        .carousel-inner>.row-equal.prev {
            display: flex;
        }

        /* prevent flicker during transition */
        .carousel-inner>.row-equal.active.left, 
        .carousel-inner>.row-equal.active.right {
            opacity: 0.5;
            display: flex;
        }


        /* control image height */
        .card-img-top-250 {
            max-height: 250px;
            overflow:hidden;
        }
    </style>
</head>

<body>
    <!-- top bar -->
    <div id="top" class="top-bar">
        <div class="container">
            <div class="row">
                <div class="col-md-12"><span class="fa fa-truck"></span> <strong>Zona Despacho:</strong> Los Andes - Calle Larga - San Esteban - San Felipe | Consulta otras zonas de despacho | Despacho Gratis por sobre 2 recargas.</div>
            </div>
        </div>
    </div>
    <!-- Navigation -->
    <nav id="mainNav" class="navbar navbar-expand-lg navbar-light shadow">
        <?php include 'navbar.php';?>
    </nav>
    <!-- Header -->
    <section id="home" class="hero space-md bg-image">
        <div class="container">
            <div class="row d-flex align-items-center justify-content-center text-center">
                <div class="col-md-10">
                    <div class="title-content-warpper">
                        <h1>Agua 110% Purificada</h1>
                    </div>
                    <h4 class="mt-3 mb-3"> Esta es la mejor agua purificada para tu hogar y empresa</h4>
                    <p>
                        <a href="#" class="btn btn-primary ml-auto">Realizar Pedido <span class="fa fa-file-alt"></span> </a>
                        <!-- <a href="#" class="btn btn-dark">Pagar Pedido <span class="fa fa-question-circle"></span> </a> -->
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-dark" data-toggle="modal" data-target="#modalPago"> Pagar Pedido <span class="fa fa-dollar-sign"></span>
                    </button>
                    </p>
                    <!-- Modal Pago WebPay by Jesus Caballero -->
                    <div class="modal fade" id="modalPago" tabindex="-1" role="dialog" aria-labelledby="labelModalPago" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="labelModalPago">Pagar Pedido</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form id="formBuscar">
                                        <div class="form-group">
                                            <input type="hidden" id="tokenOl" name="token_ol" value="<?php $date = date('h:i'); $token = hash('sha256',$date); echo $token;?>">
                                            <input type="hidden" id="dateTransaction" name="transactionDate" value="<?php $date = date('m-d h:i');?>">
                                            <input type="text" id="numberOrder" name="number_order" placeholder="Ingresa el n° de pedido entregado..." class="form-control">
                                            <button type="submit" class="btn btn-secondary">Mostrar Pedido <i class="far fa-eye"></i></button>
                                        </div>
                                            
                                    </form>
                                        <span id="loading"><img src="images/loader.gif"> Cargando...</span>
                                        <div id="load-order"></div>
                                            
                                </div>
                                <div class="modal-footer">
                                    <button type="button" id="btnCerrar" class="btn btn-secondary pull-left" data-dismiss="modal">Cerrar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Cierre Modal Pago by Jesus Caballero -->
                </div>
            </div>
            <!-- intro feature box -->
            <div class="mt-5 mb-5">
                <div class="row text-center">
                    <div class="col-12 col-md-4">
                        <div class="feature-box">
                            <div class="mb-2">
                                <div class="icon display-3 mb-3"><img src="images/icon-olimpia-wsp.png"></div>
                                <h5>Pedido Express</h5>
                                <p>¿Te quedaste sin agua? <br>Puedes hacer tu pedido express al <br>+569 5555 5555</p>
                            </div>
                        </div>
                        <!-- // end of Feture box -->
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="feature-box">
                            <div class="mb-2">
                                <div class="icon display-3 mb-3"><img src="images/icon-olimpia-pureza.png"></div>
                                <h5>Agua Purificada</h5>
                                <p>Aguas Olimpia es agua 110% purificada, libre de sodio y muy saludable para tu organismo.</p>
                            </div>
                        </div>
                        <!-- // end of Feture box -->
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="feature-box">
                            <div class="mb-2">
                                <div class="icon display-3 mb-3"><img src="images/icon-olimpia-2.png"></div>
                                <h5>Despacho Gratis</h5>
                                <p>Solicita 2 recargas de agua y el despacho es gratis, dentro de Los Andes.</p>
                            </div>
                        </div>
                        <!-- // end of Feture box -->
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- info section -->
    <section id="hogar" class="">
        <div class="w-90 bg-light space-md">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-5">
                        <div class="side-feature md-m-25px-b">
                            <div class="feature-content">
                                <label>Aguas Olimpia</label>
                                <h1 class="black-color font-alt">¿Quieres Agua Olimpia para tu hogar?</h1>
                                <div class="line">
                                    <p>Si necesitas agua 110% purificada para tu hogar y a un precio especial, solicita tus botellones de forma rápida, sencilla y en pocos pasos.</p>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 form-group">
                                        <input class="form-control" name="name_client" placeholder="Su nombre completo">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <input class="form-control" name="name_client" placeholder="Su número telefonico">
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <input class="form-control" name="name_client" placeholder="Su dirección de despacho">
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <a href="" class="btn btn-primary"><span class="fa fa-truck"></span> Solicitar Despacho</a>
                                    </div>
                                </div>
                                <small class="mt-3 d-block">Paga<span class="text-primary"> tu pedido online</span> con Webpay, Tarjetas de Crédito y Débito.</small>
                            </div>
                        </div>
                    </div>
                    <!-- col -->
                    <div class="col-lg-7 text-md-right text-sm-center"> <img src="images/repartidor-img.png" title="" alt="" class="img-fluid"> </div>
                </div>
                <!-- row -->
            </div>
            <!-- container -->
        </div>
    </section>
    <section class="carousel slide" data-ride="carousel" id="postsCarousel">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-md-right lead">
                    <a class="btn btn-outline-secondary prev" href="" title="go back"><i class="fa fa-lg fa-chevron-left"></i></a>
                    <a class="btn btn-outline-secondary next" href="" title="more"><i class="fa fa-lg fa-chevron-right"></i></a>
                </div>
            </div>
        </div>
        <div class="container p-t-0 m-t-2 carousel-inner">
            <div class="row row-equal carousel-item active m-t-0">
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-img-top card-img-top-250">
                            <img class="img-fluid" src="http://i.imgur.com/EW5FgJM.png" alt="Carousel 1">
                        </div>
                        <div class="card-block p-t-2">
                            <h6 class="small text-wide p-b-2">Insight</h6>
                            <h2>
                                <a href>Why Stuff Happens Every Year.</a>
                            </h2>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-img-top card-img-top-250">
                            <img class="img-fluid" src="http://i.imgur.com/Hw7sWGU.png" alt="Carousel 2">
                        </div>
                        <div class="card-block p-t-2">
                            <h6 class="small text-wide p-b-2">Development</h6>
                            <h2>
                                <a href>How to Make Every Line Count.</a>
                            </h2>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-img-top card-img-top-250">
                            <img class="img-fluid" src="http://i.imgur.com/g27lAMl.png" alt="Carousel 3">
                        </div>
                        <div class="card-block p-t-2">
                            <h6 class="small text-wide p-b-2">Design</h6>
                            <h2>
                                <a href>Responsive is Essential.</a>
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row row-equal carousel-item m-t-0">
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-img-top card-img-top-250">
                            <img class="img-fluid" src="//visualhunt.com/photos/l/1/office-student-work-study.jpg" alt="Carousel 4">
                        </div>
                        <div class="card-block p-t-2">
                            <h6 class="small text-wide p-b-2">Another</h6>
                            <h2>
                                <a href>Tagline or Call-to-action.</a>
                            </h2>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-img-top card-img-top-250">
                            <img class="img-fluid" src="//visualhunt.com/photos/l/1/working-woman-technology-computer.jpg" alt="Carousel 5">
                        </div>
                        <div class="card-block p-t-2">
                            <h6 class="small text-wide p-b-2"><span class="pull-xs-right">12.04</span> Category 1</h6>
                            <h2>
                                <a href>This is a Blog Title.</a>
                            </h2>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 fadeIn wow">
                    <div class="card">
                        <div class="card-img-top card-img-top-250">
                            <img class="img-fluid" src="//visualhunt.com/photos/l/1/people-office-team-collaboration.jpg" alt="Carousel 6">
                        </div>
                        <div class="card-block p-t-2">
                            <h6 class="small text-wide p-b-2">Category 3</h6>
                            <h2>
                                <a href>Catchy Title of a Blog Post.</a>
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- section with customer feedbacks -->
    <div class="space-md w-90 bg-primary">
        <div class="container">
            <div class="row align-items-md-center">
                <div class="col-md-6">
                    <div class="video-cover rounded shadow-md"> <img alt="Image" src="images/pexels-photo-1065084.jpeg" class="img-fluid">
                        <a class="play-video video-play-button" href="#"> <span></span> </a>
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" data-src="#" allowfullscreen="" allow="autoplay; encrypted-media"></iframe>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <blockquote class="testimonial-block waypoint-reveal">
                        <div class="testimonial-quote zoom-in">
                            <svg viewBox="0 0 109.253418 82.4897461" enable-background="new 0 0 500 500" xml:space="preserve">
                                <path style="fill:#084afa;" d="M27.496582,82.4897461C10.6318359,82.4897461,0,67.4584961,0,46.9277344 C0,12.831543,18.3310547,0.3662109,47.2939453,0v12.831543c-13.5649414,0-22.7304688,5.4995117-25.296875,15.0317383 c-2.9331055,6.5991211-2.5664062,11.3652344-2.199707,15.03125c1.8330078-1.8330078,5.8662109-2.9326172,8.7988281-2.9326172 c12.8320312,0,21.2641602,9.1655273,21.2641602,20.8974609S40.6948242,82.4897461,27.496582,82.4897461z M87.2558594,82.4897461 c-16.8642578,0-27.8632812-15.03125-27.8632812-35.5620117C59.3925781,12.831543,78.090332,0.3662109,106.6870117,0v12.831543 c-13.1982422,0-22.3637695,5.4995117-25.296875,15.0317383c-2.5664062,6.5991211-2.5664062,11.3652344-1.8330078,15.03125 c1.8330078-1.8330078,5.4990234-2.9326172,8.7988281-2.9326172c12.465332,0,20.8974609,9.1655273,20.8974609,20.8974609 S100.4545898,82.4897461,87.2558594,82.4897461z"></path>
                            </svg>
                        </div>
                        <div class="testimonial-block-copy step-up">
                            <p class="h3 font-white">Their skill and expertise in digital design and development is self evident. I knew the finished product would be the envy of our competitors. A true partnership that's taken our digital business to the next level.</p>
                        </div>
                        <footer class="step-up"><cite>Gavin Feilding – Digital Manager, dusk Australia</cite></footer>
                    </blockquote>
                </div>
            </div>
        </div>
    </div>
    
    <div class="space-md" id="service">
        <div class="container">
            <div class="row d-flex justify-content-center mb-md-4 mb-sm-3">
                <div class="col-md-9 text-center">
                    <p class="-label">Integrations</p>
                    <h3 class="h1">Stop tearing your hair out over seemingly incomprehensible analytics reports.</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 mb-4">
                    <div class="card equal-heigth mt-4">
                        <div class="card-body">
                            <div class="mb-2 mt-2"> <span class="h6">Inspiration</span> </div>
                            <div class="mb-5">
                                <h3 class="h5">The most popular HTML, CSS, and JS framework.</h3>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="d-flex align-items-center justify-content-between"> <a href="#" class="hover-link">More Information <i class="fa fa-arrow-right"></i></a> </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mb-4">
                    <div class="card equal-heigth mt-4">
                        <div class="card-body">
                            <div class="mb-2 mt-2"> <span class="h6">Inspiration</span> </div>
                            <div class="mb-5">
                                <h3 class="h5">Works great in all modern browsers.</h3>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="d-flex align-items-center justify-content-between"> <a href="#" class="hover-link">More Information <i class="fa fa-arrow-right"></i></a> </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mb-4">
                    <div class="card equal-heigth mt-4">
                        <div class="card-body">
                            <div class="mb-2 mt-2"> <span class="h6">Inspiration</span> </div>
                            <div class="mb-5">
                                <h3 class="h5">Create awesome mobile-friendly websites! Variety of sample landing pages. </h3>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="d-flex align-items-center justify-content-between"> <a href="#" class="hover-link">More Information <i class="fa fa-arrow-right"></i></a> </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 mb-4">
                    <div class="card equal-heigth mt-4">
                        <div class="card-body">
                            <div class="mb-2 mt-2"> <span class="h6">Inspiration</span> </div>
                            <div class="mb-5">
                                <h3 class="h5">The most popular HTML, CSS, and JS framework.</h3>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="d-flex align-items-center justify-content-between"> <a href="#" class="hover-link">More Information <i class="fa fa-arrow-right"></i></a> </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mb-4">
                    <div class="card equal-heigth mt-4">
                        <div class="card-body">
                            <div class="mb-2 mt-2"> <span class="h6">Inspiration</span> </div>
                            <div class="mb-5">
                                <h3 class="h5">Works great in all modern browsers.</h3>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="d-flex align-items-center justify-content-between"> <a href="#" class="hover-link">More Information <i class="fa fa-arrow-right"></i></a> </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mb-4">
                    <div class="card equal-heigth mt-4">
                        <div class="card-body">
                            <div class="mb-2 mt-2"> <span class="h6">Inspiration</span> </div>
                            <div class="mb-5">
                                <h3 class="h5">Create awesome mobile-friendly websites! Variety of sample landing pages. </h3>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="d-flex align-items-center justify-content-between"> <a href="#" class="hover-link">More Information <i class="fa fa-arrow-right"></i></a> </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- cta section -->
    <section class="text-center w-90 border-bottom border-left border-right">
        <div class="container">
            <div class="row">
                <div class="col-md-12 p-4"> <span class="label label--inline">Hot!</span> <span>Get this template for free.Visit ShareBootstrap.com
                        <a href="https://sharebootstrap.com">Buy it</a> for $0 USD.</span> </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
    <!-- Pricing section -->
    <div class="space-md" id="pricing">
        <div class="container">
            <div class="row d-flex justify-content-center text-center">
                <div class="col-md-10 mb-md-5 mb-sm-3">
                    <p class="-label"> Pricing </p>
                    <h2>No additional costs.
                        Pay for what you use.</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="card pricing-v3 p-md-5 p-sm-3 text-center">
                        <div class="v3-row">
                            <p class="mb-3">Starter package</p>
                            <h3 class="display-3 font-weight-bold">$18</h3>
                            <p>Regular License</p>
                        </div> <a class="btn btn-outline-primary" href="#">Start Free Trial</a>
                        <div class="mt-4">
                            <p class="text-dark mb-2">Files:</p>
                            <p class="">.CSS .HTML .JS .SVG</p>
                            <p class="my-2 small badge badge-primary">Licensed for 10 projects</p>
                        </div>
                    </div>
                    <!-- end of pricing -->
                </div>
                <!-- end of col  -->
                <div class="col-md-4">
                    <div class="card pricing-v3 p-md-5 p-sm-3 text-center">
                        <div class="v3-row">
                            <p class="mb-3">Ultimate package</p>
                            <h3 class="display-3 font-weight-bold">$39</h3>
                            <p>Extended License</p>
                        </div> <a class="btn btn-gradient" href="#">Start Free Trial</a>
                        <div class="mt-4">
                            <p class="text-dark mb-2">Files:</p>
                            <p class="">.CSS .HTML .JS .SVG</p>
                            <p class="my-2 small badge badge-primary">Unlimited usage</p>
                        </div>
                    </div>
                    <!-- end of pricing -->
                </div>
                <!-- end of col  -->
                <div class="col-md-4">
                    <div class="card pricing-v3 p-md-5 p-sm-3 text-center">
                        <div class="v3-row">
                            <p class="mb-3">Premium package</p>
                            <h3 class="display-3 font-weight-bold">$26</h3>
                            <p>Regular License</p>
                        </div> <a class="btn btn-outline-primary" href="#">Start Free Trial</a>
                        <div class="mt-4">
                            <p class="text-dark mb-2">Files:</p>
                            <p class="">.CSS .HTML .JS .SVG</p>
                            <p class="my-2 small badge badge-primary">Licensed for 50 projects</p>
                        </div>
                    </div>
                    <!-- end of pricing -->
                </div>
                <!-- end of col  -->
            </div>
        </div>
    </div>
    <!-- Cta with singup form -->
    <section class="space-md border-bottom bg-light" id="signup">
        <div class="mask bg-dark"></div>
        <div class="container">
            <div class="row d-flex justify-content-center text-center">
                <div class="col-lg-8">
                    <div class="w-85">
                        <h2 class=""> SignUp for a free Trial </h2>
                        <p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua..</p>
                        <div class="row row d-flex justify-content-center mt-5">
                            <div class="col-md-8">
                                <!-- Input -->
                                <div class="form-message mb-3">
                                    <div class="input-group input-group form">
                                        <div class="input-group-prepend form__prepend"> <span class="input-group-text form__text">
                                                <span class="fa fa-envelope form__text-inner"></span> </span>
                                        </div>
                                        <input type="email" class="form-control form__input" name="signupEmail" required="" placeholder="Enter your email address" aria-label="Enter your email address">
                                    </div>
                                </div>
                                <!-- End Input -->
                                <!-- Input -->
                                <div class="form-message mb-3">
                                    <div class="input-group input-group form">
                                        <div class="input-group-prepend form__prepend"> <span class="input-group-text form__text">
                                                <span class="fa fa-lock form__text-inner"></span> </span>
                                        </div>
                                        <input type="password" class="form-control form__input" name="signupPassword" required="" placeholder="Create your password" aria-label="Create your password">
                                    </div>
                                </div>
                                <!-- End Input -->
                                <!-- Input -->
                                <div class="form-message mb-3">
                                    <div class="input-group input-group form">
                                        <div class="input-group-prepend form__prepend"> <span class="input-group-text form__text">
                                                <span class="fa fa-phone form__text-inner"></span> </span>
                                        </div>
                                        <input type="text" class="form-control form__input" name="signupPhoneNumber" required="" placeholder="Enter mobile number" aria-label="Enter mobile number">
                                    </div>
                                </div>
                                <!-- End Input -->
                                <div class="form-group text-left pt-2 pb-2"> <span class="lead-text">By joining, you agree to the <a href="#">Terms &amp; Conditions</a> and <a href="#">Privacy Policy.</a></span> </div>
                                <button type="submit" class="btn btn-block btn-gradient">Sign Up</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Section - client logos -->
    <section class="container clients py-5">
        <div class="row text-center">
            <!-- Logo -->
            <div class="col-lg-3 col-sm-3 col-xs-12">
                <div class="hero-img"> <img src="images/logo-freshdesk.svg" alt="" class="img-fluid"> </div>
            </div>
            <!-- Logo -->
            <div class="col-lg-3 col-sm-3 col-xs-12">
                <div class="hero-img"> <img src="images/logo-helpscout.svg" alt="" class="img-fluid"> </div>
            </div>
            <!-- Logo -->
            <div class="col-lg-3 col-sm-3 col-xs-12">
                <div class="hero-img"> <img src="images/logo-intercom.svg" alt="" class="img-fluid"> </div>
            </div>
            <!-- Logo -->
            <div class="col-lg-3 col-sm-3 col-xs-12">
                <div class="hero-img"> <img src="images/logo-kustomer.svg" alt="" class="img-fluid"> </div>
            </div>
        </div>
    </section>
    <!-- Footer -->
    <footer class="pt-5 pb-1 bg-dark">
        <div class="container">
            <div class="row">
                <div class="col-lg-auto">
                    <a class="d-inline-block mb-3 mr-4 text-light" href="#">Service</a>
                    <a class="d-inline-block mb-3 mr-4 text-light" href="#">About</a>
                    <a class="color-3 d-inline-block mb-3 mr-4 text-light" href="#">Pricing</a>
                    <a class="d-inline-block mb-3 mr-4 text-light" href="#">Contact</a>
                </div>

                <div class="col-12">
                    <hr class="my-2">
                </div>
                <div class="col-lg-auto mt-4">
                    <p class="text-white"><img src="images/logo-olimpia-footer.png"><small>Una empresa <a href="#" class="text-white">Parentesis</a></small></p>
                </div>

                <div class="col-lg-auto ml-lg-auto mt-4"><a class="d-inline-block mb-3 mr-4 text-light" href="https://agrafica.cl" target="_blank">Agrafica</a><a class="d-inline-block mb-3 mr-4 text-light" href="https://libreriaparentesis.cl" target="_blank">Parentesis Ltda</a>
                    <a class="color-4 mr-4" href="#"></a>
                </div>
            </div>
            <!--/.row-->
        </div>
        <!--/.container-->
    </footer>
    <!-- Footer -->
    <footer class="footer text-center">
        <div class="container"> </div>
    </footer>
    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Custom scripts for this template -->
    <script src="js/style.js"></script>
    <script>
        $(document).ajaxStart(function() {
            $("#loading").show();
            }).ajaxStop(function() {
            $("#loading").hide();
            });

        $(document).ready(function() {
            $("#formBuscar").on( "submit", function(e) {
            e.preventDefault();
                $.ajax({
                    url:'processOrder.php',
                    method:'POST',
                    data: $(this).serialize(),
                    dataType:'html',
                    success: function(data) {
                        //Cargamos finalmente el contenido deseado
                        $('#load-order').fadeIn(1500).html(data);
                    }
                });
            });              
        }); 

        $("#btnCerrar").on("click", function(e){
            e.preventDefault();
            $( "#formBuscar" ).trigger("reset");
        });
        
    </script>

</body>
</html>