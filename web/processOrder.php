<?php
  /*-------------------------
  Autor: Jesús Caballero P.
  Facebook: https://www.facebook.com/unicoescritorbeats
  Mail: uebeats@gmail.com
  ---------------------------*/
    /* Conectar con Base de datos */
    require_once 'includes/conexion.php';//Contiene funcion que conecta a la base de datos

    /* Se ejecuta un require_once a la libreria SDK de TRANSBANK */
    require_once './vendor/autoload.php';

    /* Importamos las clases */
    use Transbank\Webpay\Webpay;
    use Transbank\Webpay\Configuration;

    /* Recibimos las variables por $.ajax */
    $token = $_POST['token_ol'];
    $number = $_POST['number_order'];


    /* Variable $html vacia */
    $html = "";

    if((int)$number){
        /* Ejecutamos consulta a la tabla order_water para validar $number */
        $q = "SELECT number_order FROM order_water WHERE number_order = $number";
        $r = $con->query($q);
        $rw = $r->fetch_assoc();
        $num_rw = $rw['number_order'];
    }

    /* Ejecutamos consulta a la tabla order_water */
    if (!empty($num_rw) && $num_rw == $number) {
        $query = "SELECT * FROM order_water WHERE number_order = $number";
        $resultado = $con->query($query);
        $row = $resultado->fetch_assoc();

    /* Formateamos $number */
        $m = $row['amount_order'];
        $mf = number_format($m, 0, ",", ".");

    /* Verificamos el $status_order */
        $status_order = $row['status_order'];

        if($status_order == 'pagado'){
            $status ='<span id="statusPay" class="badge badge-pill badge-success">';
        } else {
            $status ='<span id="statusPen" class="badge badge-pill badge-danger">';
        }

        if($status_order == 'pagado'){
            $statusBtn ='disabled="disabled"';
        } else {
            $statusBtn ='';
        }

    /* Inicializacion de $transaction Webpay */
        $transaction = (new Webpay(Configuration::forTestingWebpayPlusNormal()))
        ->getNormalTransaction();

        $amount = $row['amount_order'];
        $sessionId = 'sessionId';
        $buyOrder = $row['number_order'];
        $returnUrl = 'http://localhost:80/aguasolimpia/web/returnPay.php';
        $finalUrl = 'http://localhost:80/aguasolimpia/web/finalPay.php';

        $initResult = $transaction->initTransaction(
        $amount, $buyOrder, $sessionId, $returnUrl, $finalUrl);

        $formAction = $initResult->url;
        $tokenWs = $initResult->token;


    /* Variable $html con contenido */
        $html .= '
        <table class="table table-striped">
            <thead id="tblHead">
                <tr>
                    <th class="text-left">Pedido para:</th>
                    <th class="text-left">Dirección:</th>
                    <th class="text-left">Monto ($):</th>
                </tr>
            </thead>
            <tbody>
                <tr><td>'. $row['name_client'] .'</td>
                    <td>'. $row['address_order'] .'</td>
                    <td>$'. $mf .'<br>'. $status . $row['status_order'] .'</span></td>
                </tr>
            </tbody>
        </table>
        <div class="row">
            <div class="col-12">
                <form action="'. $formAction .'" method="POST">
                    <input name="token_ws" type="hidden" value="'. $tokenWs .'">
                    <button '. $statusBtn . ' class="btn btn-secondary"><img src="images/webpayplus.png" width="80"> Pagar con Webpay</button>
                </form>
            </div>
        </div>';

    echo $html;

        // echo $row['address_order'];

    } else {
        echo 'No existe n° pedido o el ingreso no es un n°';
    }
    
?>