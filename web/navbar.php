<div class="container"> 
    <a class="navbar-brand js-scroll-trigger" href="#top"><img src="images/logo-olimpia-web.png"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item"> <a class="nav-link js-scroll-trigger font-menu" href="#home">Inicio</a> </li>
            <li class="nav-item"> <a class="nav-link js-scroll-trigger font-menu" href="#hogar">Olimpia Hogar</a> </li>
            <li class="nav-item"> <a class="nav-link js-scroll-trigger font-menu" href="#empresa">Olimpia Empresa</a> </li>
            <li class="nav-item"> <a class="nav-link js-scroll-trigger font-menu" href="#contacto">Contacto</a> </li>
        </ul>
    </div> 
    <a href="#" class="btn btn-primary">Realizar Pedido <span class="fa fa-file-alt"></span> </a></p>
</div>