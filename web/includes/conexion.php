<?php
    /*-------------------------
    Autor: Jesus Caballero
    Web: agrafica.cl
    Mail: uebeats@gmail.com
    ---------------------------*/
    // DB credentials.
    define('DB_HOST','localhost');
    define('DB_USER','root');
    define('DB_PASS','');
    define('DB_NAME','db_olimpia_web');

    global $con;
    # conectare la base de datos
    $con = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
    if(!$con){
        die("imposible conectarse: ".mysqli_error($con));
    }
    if (@mysqli_connect_errno()) {
        die("Conexion fallo: ".mysqli_connect_errno()." : ". mysqli_connect_error());
    }
    $con->set_charset('utf8');
?>